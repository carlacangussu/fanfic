Fanfic::Application.routes.draw do
  devise_for :users
  resources :posts do
    resources :comentarios
  end
  root :to => "posts#index"
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
