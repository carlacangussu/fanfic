class Post < ActiveRecord::Base
  has_many :comentarios
  validates :titulo, :presence => true
  validates :conteudo, :presence => true, :length => { :minimum => 10 }
end
